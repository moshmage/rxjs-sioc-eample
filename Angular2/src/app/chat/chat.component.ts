import { Component, OnInit,OnDestroy } from '@angular/core';
import {IO, ioEvent} from 'rxjs-socket.io';

@Component({
  selector: 'chat-component',
  templateUrl: "./chat.component.html",
})
export class ChatComponent implements OnInit, OnDestroy {
  private url: string = "http://localhost:1337"; /** in real life, this should come from a configuration provider */
  private connection;
  private _nickname;

  private messageSubscription;
  private onMessage: ioEvent;

  private nicknameSubscription;
  private onNickInUse: ioEvent;

  public messages: {message: string, from: string}[] = [];
  public connected: boolean = false;
  public nickname: string = '';
  public warning: string = '';
  public message: string = '';

  constructor(private socket:IO) {
    /** in real life, these would be in a provider, extending ioEvent or not, and
     * would be loaded via constructor */
    this.onMessage = new ioEvent({name: 'message'});
    this.onNickInUse = new ioEvent({name: 'nickname'});

    socket.listenToEvent(this.onMessage);
    socket.listenToEvent(this.onNickInUse);
  }

  connect(nickname: string) {
    if (!nickname) {
      this.warning = 'write a nickname first';
      return;
    }

    if (this.warning) this.warning = '';
    this.socket.connect(this.url);
  }

  sendMessage(string){
    if (!string) return;
    this.socket.emit('message', {from: this.nickname, message: string});
    this.messages.push({from: this.nickname, message: string});
    this.message = '';
  }

  ngOnInit() {

    this.connection = this.socket.event$.subscribe((newConnectionData) => {
      if (!this.connected && newConnectionData.connected) this.socket.emit('nickname', this.nickname);
      this.connected = newConnectionData.connected === true;

    });

    this.messageSubscription = this.onMessage.event$.subscribe(message => {
      this.messages.push(message);
    });

    this.nicknameSubscription = this.onNickInUse.event$.subscribe(data => {
      console.log('nickname-in-use', data);
      this.nickname = (data.inUse) ? data.nickname : this.nickname;
    });
  }

  ngOnDestroy() {
    this.connection.unsubscribe();
  }
}
