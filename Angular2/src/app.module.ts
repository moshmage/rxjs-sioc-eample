import {NgModule} from '@angular/core';
import {ClientAppComponent} from './app/client.component';
import {ChatComponent} from './app/chat/chat.component';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {IO} from 'rxjs-socket.io';

@NgModule({
  declarations: [ClientAppComponent, ChatComponent],
  bootstrap: [ClientAppComponent],
  imports: [BrowserModule, FormsModule],
  providers: [IO]
})
export class AppModule {
}
