/**
 * Created by Mosh Mage on 12/19/2016.
 */
var io = require('socket.io');
var server = io.listen(1337);

var _nicks = [];
function nickExists(nickname) { return _nicks.indexOf(nickname) > -1; }

server.on('connection', function (socket) {

    socket.on('nickname', function(nickname) {
        console.log('user joined', nickExists(nickname) ? socket.id : nickname);
        var exists = nickExists(nickname);
        if (!exists) {
            _nicks.push(nickname);
            socket.nickname = nickname;
        } else {
            _nicks.push(socket.id);
            socket.nickname = socket.id;
        }
        socket.emit('nickname', {inUse: exists, nickname: socket.nickname });
    });

    socket.on('message', function (message) {
        socket.broadcast.emit('message', message);
    });

    socket.on('disconnect', function () {
        var index = _nicks.indexOf(socket.nickname);
        if (index > -1) _nicks.splice(index,1);

        console.log('user disconnected:', socket.nickname);
    });
});


